#include "forceField.h"

forceField::forceField() {

}


forceField::forceField(float mass) {
    this->mass = mass;
}


ofVec2f forceField::castForce(float x, float y, fallen f) {
    position.x = x;
    position.y = y;

    ofVec2f forceDir = position - f.getPosition();
    float d = forceDir.length();
    d = ofClamp(d, 5, 25);
    forceDir.normalize();

    float strength = (G * mass * f.getMass() ) / (d*d);
    forceDir *= strength;

    return forceDir;
}

bool forceField::inRange(ofVec2f target) {
    ofVec2f a = target;
    ofVec2f b = position;
    float dist = (a-b).length();

    if(dist > 75 && dist < 100) {
        ofNoFill();
        ofLine(a.x, a.y, b.x, b.y);
        ofCircle(a.x, a.y, 5);
        ofCircle(b.x, b.y, 5);
        return true;
    }

    return false;
}

