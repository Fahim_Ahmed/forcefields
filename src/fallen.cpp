#include "fallen.h"

fallen::fallen() {
}

fallen::fallen(float x, float y, float radius, float mass) {
    this->position.x = x;
    this->position.y = y;
    this->radius = radius;
    this->mass = mass;
//    this->frictionCoefficient = frictionCoefficient;
}

void fallen::update() {
    velocity += acceration;

    if(velocity.length() > MAX_LENGTH){
        velocity.normalize();
        velocity *= MAX_LENGTH;
    }

    position += velocity;
    acceration *= 0;

//    printf("%f", velocity.x);
}

void fallen::applyForce(ofVec2f force) {
    //printf("Applying force!");
    acceration = acceration + (force/mass);
}

void fallen::draw(ofColor color) {
    //ofSetColor(210);
    ofSetColor(color);
    ofCircle(position.x, position.y, radius);
}

void fallen::stop(){
    velocity *= 0;
}

ofVec2f fallen::getVelocity() {
    return velocity;
}

float fallen::getMass() {
    return mass;
}

ofVec2f fallen::getPosition() {
    return position;
}

void fallen::edgeBounce() {
    if (position.x > ofGetWindowWidth()) {
      velocity.x *= -1;
    }
    else if (position.x < 0) {
      velocity.x *= -1;
    }

    if (position.y > ofGetWindowHeight()) {
      velocity.y *= -1;
    }
    else if (position.y < 0) {
      velocity.y *= -1;
    }
}

void fallen::checkEdge() {
    if (position.x > ofGetWindowWidth()) {
      position.x = 0;
    }
    else if (position.x < 0) {
      position.x = ofGetWindowWidth();
    }

    if (position.y > ofGetWindowHeight()) {
      position.y = 0;
    }
    else if (position.y < 0) {
      position.y = ofGetWindowHeight();
    }
}
