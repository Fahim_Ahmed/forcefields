#ifndef FALLEN_H
#define FALLEN_H

#include "ofMain.h"

#define MAX_LENGTH 25

class fallen {
    public:
        fallen();
        fallen(float x, float y, float radius, float mass);

        void update();
        void applyForce(ofVec2f force);
        void draw(ofColor color);
        void checkEdge();
        void edgeBounce();
        void stop();

        ofVec2f getVelocity();
        ofVec2f getPosition();

        float getMass();

    protected:
    private:
        float mass;
        float radius;
//        float frictionCoefficient;
        ofVec2f position;
        ofVec2f velocity;
        ofVec2f acceration;

};

#endif // FALLEN_H
