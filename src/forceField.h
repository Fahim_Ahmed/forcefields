#ifndef FORCEFIELD_H
#define FORCEFIELD_H

#include "ofMain.h"
#include "fallen.h"

#define G 9.8


class forceField : public ofBaseApp {

    public:
        forceField();
        forceField(float mass);

        ofVec2f castForce(float x, float y, fallen f);
        bool inRange(ofVec2f target);


    protected:

    private:
        float mass;
        float radius;

        ofVec2f position;
        ofVec2f velocity;
        ofVec2f acceration;
};

#endif // FORCEFIELD_H
