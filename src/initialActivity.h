#ifndef INITIALACTIVITY_H
#define INITIALACTIVITY_H

#include <ofMain.h>
//#include "fallen.h"
#include "forceField.h"


class initialActivity : public ofBaseApp {
    public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

    protected:

    private:
        fallen** _fallens;
        forceField *_forceField;
        ofImage gradImage;

        int numFallen;
        float frictionCoefficient;

        ofVec2f friction;

        bool forceOn = false, fallenInRange = false;

        ofVec2f calculateFriction(float c, fallen target);
        void ofDrawFPS(int x, int y);
};

#endif // INITIALACTIVITY_H
