#include "initialActivity.h"

#define FRICTION_COEEFFICIENT 0.1
#define IMG_NAME "grad1.png"
#define IMG_WIDTH 1000
#define IMG_HEIGHT 200

void initialActivity::setup(){
    ofSetFrameRate(30);
	ofBackground(0);
	//ofBackground(255, 146, 0);
	//ofEnableSmoothing();

    frictionCoefficient = FRICTION_COEEFFICIENT;

	_forceField = new forceField(20);

	gradImage.loadImage(IMG_NAME);

	int ox = 10;
	int oy = 10;
	int xNum = ofGetWindowWidth()/ox - 1;
	int yNum = ofGetWindowHeight()/oy - 1;

	numFallen = xNum * yNum;

    _fallens = new fallen*[numFallen];

	for (int i = 0; i < yNum; i++) {
        for (int j = 0; j < xNum; j++) {
            fallen *f = new fallen(j*ox + ox, i*oy + oy, 1, 1);
            _fallens[i*xNum + j] = f;
            //printf("%i ", j*ox + ox);
        }
	}
}

//--------------------------------------------------------------
void initialActivity::update(){

    for (int i = 0; i < numFallen; i++){
        _fallens[i]->update();

        if(_fallens[i]->getVelocity().length() >= 0.005)
            _fallens[i]->applyForce(calculateFriction(frictionCoefficient, *_fallens[i]));
        else{
            _fallens[i]->stop();
        }
    }

//    printf("%f \n", _fallens[0]->getVelocity().x);

    if(forceOn){
        for (int i = 0; i < numFallen; i++){
            ofVec2f f = _forceField->castForce(mouseX, mouseY, *_fallens[i]);
            //fallenInRange = _forceField.inRange(_fallens[i]->getPosition());
            _fallens[i]->applyForce(f);

        }
    }
}

//--------------------------------------------------------------
void initialActivity::draw(){
    for (int i = 0; i < numFallen; i++) {
        float speed = _fallens[i]->getVelocity().length();
        float xPos = ofMap(speed, 0, MAX_LENGTH, 0, IMG_WIDTH - 1, false);
        ofColor color = gradImage.getColor(xPos, 10);

        _fallens[i]->draw(color);
        _fallens[i]->checkEdge();
        //_fallens[i]->edgeBounce();
    }
   // ofDrawFPS(15, 150);
}

ofVec2f initialActivity::calculateFriction(float c, fallen target) {
    friction = target.getVelocity();
    friction *= -1;
    friction.normalize();
    friction *= c;

    return friction;
}

void initialActivity::ofDrawFPS(int x, int y) {
	glPushMatrix();
	glTranslatef(x, y, 0);
	ofSetRectMode(OF_RECTMODE_CORNER);
	ofFill();
	ofSetColor(0x000000);
	ofSetColor(0, 255, 0);
	string fpsStr = "FPS: "+ofToString(ofGetFrameRate(), 2);
	ofDrawBitmapString(fpsStr, 0, 0);
	glPopMatrix();

	ofSetColor(255);
}

//--------------------------------------------------------------
void initialActivity::keyPressed(int key){

}

//--------------------------------------------------------------
void initialActivity::keyReleased(int key){
    ofVec2f force(5,0);
    for (int i = 0; i < numFallen; i++){
        _fallens[i]->applyForce(force);
    }
}

//--------------------------------------------------------------
void initialActivity::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void initialActivity::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void initialActivity::mousePressed(int x, int y, int button){
    forceOn = true;
}

//--------------------------------------------------------------
void initialActivity::mouseReleased(int x, int y, int button){
    forceOn = false;
}

//--------------------------------------------------------------
void initialActivity::windowResized(int w, int h){

}

//--------------------------------------------------------------
void initialActivity::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void initialActivity::dragEvent(ofDragInfo dragInfo){

}
